import React from "react";
import PageImage from "../../pages/PageImage";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";

import clientServer from "./images/fetch/03_client-server.png";
const images = { clientServer };

export default [
  {
    p: (
      <PageImage
        title="Echange client/serveur"
        subtitle="Requête Http (Ajax)"
        image={images.clientServer}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Une requête Ajax hier..."
        subtitle="Asynchronous Javascript And Xml"
        lang="js"
        source={`if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    request = new XMLHttpRequest();
  } else if (window.ActiveXObject) { // IE
    try {
      request = new ActiveXObject('Msxml2.XMLHTTP');
    }
    catch (e) {
      try {
        request = new ActiveXObject('Microsoft.XMLHTTP');
      }
      catch (e) {}
    }
  }
  
  function onLoadListener() {
    var data = JSON.parse(this.responseText);
    console.log(data);
  }
  
  function onErrorListener(err) {
    console.log('XHR Error :', err);
  }
  
  request.onload = onLoadListener;
  request.onerror = onErrorListener;
  request.open('get', 'https://api.acme.com/some.json', true);
  request.send();`}
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Fetch = une requête HTTP aujourd'hui"
        subtitle="https://fetch.spec.whatwg.org"
        lang="js"
        source={`fetch('https://api.myjson.com/bins/9l2ez')
  .then((response) => {
      return response.json();
  }).then((jsonData) => {
      console.log(jsonData);
  }).catch((err) => {
      console.log("Opps, Something went wrong!", err);
  })`}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://caniuse.com/#search=fetch">Can i use ?</Link>
        }
        subtitle={
          <Link href="https://github.com/github/fetch">
            window.fetch polyfill
          </Link>
        }
        src="https://caniuse.com/#search=fetch"
      />
    )
  }
];
