import React from "react";
import { S } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageIframe from "../../pages/PageIframe";
import PageTitle from "../../pages/PageTitle";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageQuote from "../../pages/PageQuote";
import PageCodePane from "../../pages/PageCodePane";
import Link from "../../components/Link";
import { Markdown } from "spectacle";

import fonts from "./images/rn-tools/05_fonts.png";
import typo from "./images/rn-tools/06_typography.png";
import iosGuidlines from "./images/rn-tools/07_iosGuidlines.png";
import svg from "./images/rn-tools/08_svg.png";
import svgResult from "./images/rn-tools/09_svg-result.png";
import svgr from "./images/rn-tools/10_svgr.png";
import homer from "./images/rn-tools/11_homer.png";
import swipper from "./images/rn-tools/12_swiper.gif";
import maps from "./images/rn-tools/13_maps.png";
import markers from "./images/rn-tools/14_markers.png";
import filters from "./images/rn-tools/15_filters.png";
import material from "./images/rn-tools/16_material.png";
import lottie from "./images/rn-tools/17_lottie.gif";
import design from "./images/rn-tools/18_design.png";
import bridge from "./images/rn-tools/19_bridge.png";
import uiBridge from "./images/rn-tools/20_ui-bridge.png";
import build from "./images/rn-tools/21_build.png";
const images = {
  fonts,
  typo,
  iosGuidlines,
  svg,
  svgResult,
  svgr,
  homer,
  swipper,
  maps,
  markers,
  filters,
  material,
  lottie,
  design
};
export default [
  {
    p: <PageTitle title="Styles with" subtitle="React Native" />
  },
  {
    p: (
      <PageIframe
        title="Typographie"
        subtitle={
          <Link href="https://material.io/guidelines/style/typography.html#typography-styles">
            Material design guidelines
          </Link>
        }
        src="https://material.io/guidelines/style/typography.html#typography-styles"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://developer.apple.com/ios/human-interface-guidelines/visual-design/typography">
            Apple Human Interface Guidelines
          </Link>
        }
        image={images.iosGuidlines}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/hectahertz/react-native-typography">
            React Native typography
          </Link>
        }
        subtitle="Perfect usage with San Francisco & Roboto typo"
        image={images.typo}
      />
    )
  },

  {
    p: (
      <PageIframe
        title={
          <Link href="https://hackernoon.com/all-you-need-to-know-about-css-in-js-984a72d48ebc">
            Css-in-Js !== Inline Styles
          </Link>
        }
        src="https://mxstbr.blog/2016/11/inline-styles-vs-css-in-js/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://www.styled-components.com/docs/basics#react-native">
            Styled components
          </Link>
        }
        src="https://www.styled-components.com/docs/basics#react-native"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://glamorous.rocks/basics#glamorous-native">
            Glamorous
          </Link>
        }
        src="https://glamorous.rocks/basics#glamorous-native"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://react-native-training.github.io/react-native-elements/">
            React Native Elements
          </Link>
        }
        src="https://react-native-training.github.io/react-native-elements/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://nativebase.io">React Native Base</Link>}
        src="https://nativebase.io"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/xotahal/react-native-material-ui">
            React Native Material UI
          </Link>
        }
        image={images.material}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://github.com/react-native-community/react-native-svg">
            react-native-svg
          </Link>
        }
        subtitle="Svg are not supported without library"
        image={images.svg}
      />
    )
  },
  /*{
    p: (
      <PageList
        title="Link react-native-svg library"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "1. yarn add react-native-svg@5.4.1",
          "2. react-native link react-native-svg",
          "3. yarn run ios || android"
        ]}
      />
    )
  },*/
  /*{
    p: (
      <PageImage
        title="Here we go !"
        subtitle="The power of svg in your hands"
        image={images.svgResult}
      />
    )
  },*/
  {
    p: (
      <PageImage
        title={<Link href="https://github.com/smooth-code/svgr">SVGR</Link>}
        subtitle="Transform your svg into components"
        image={images.svgr}
      />
    )
  },
  /*{
    p: (
      <PageImage
        title={
          <Link href="http://thenewcode.com/assets/images/thumbnails/homer-simpson.svg">
            homer svg file
          </Link>
        }
        subtitle="Goal"
        image={images.homer}
      />
    )
  },
  {
    p: (
      <PageList
        title="Svgr configuration"
        listTextSize="2rem"
        marginItem={10}
        points={[
          "1. Copy homer.svg in ./src/shared-ui/Icon/svg/ folder ",
          <span>
            2. add this command to your npm scripts<br />
            {'"svgr":"svgr ./src/shared-ui/Icon/svg/${SVG}.svg -d ./ --native"'}
          </span>,
          "3. launch this in your terminal: SVG=homer npm run svgr",
          "4. import your generated component"
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://js.coach/?search=swipper&collection=React+Native">
            js.coach
          </Link>
        }
        subtitle="swipper"
        src="https://js.coach/?search=swipper&collection=React+Native"
      />
    )
  },*/
  {
    p: (
      <PageIframe
        title={
          <Link href="http://www.reactnativeexpress.com/animated">
            React Native Animated
          </Link>
        }
        src="http://www.reactnativeexpress.com/animated"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/react-native/docs/performance.html">
            Performance
          </Link>
        }
        src="https://facebook.github.io/react-native/docs/performance.html"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="http://airbnb.io/lottie/">Animation with Lottie</Link>
        }
        src="http://airbnb.io/lottie/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="http://airbnb.io/lottie/react-native#getting-started">
            Lottie React native
          </Link>
        }
        src="http://airbnb.io/lottie/react-native#getting-started"
        subtitle={
          <Link href="http://airbnb.io/lottie/react-native#getting-started">
            docs
          </Link>
        }
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://www.lottiefiles.com/">Lottiefile.com</Link>}
        subttitle={
          <Link href="https://www.lottiefiles.com/">
            Download a free animation
          </Link>
        }
        src="https://www.lottiefiles.com/"
      />
    )
  },
  {
    p: <PageImage title="Exemple" image={images.lottie} />
  }
];
