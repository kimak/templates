/*eslint-disable object-shorthand*/

// Default Spectacle Theme for Screens
import screen from "spectacle/lib/themes/default/screen.js";
import { mergeObject } from "../utils";

// smoothcode
const colors = {
  primary: "#FFF", //white
  secondary: "#00adbe", //orange
  tertiary: "#000000",
  quartenary: "#3e586e"
};

const fonts = {
  primary: { name: "Roboto", googleFont: true},
  secondary: "Helvetica",
};

const defaultTheme = screen(colors, fonts);

// Custom Theme
const customTheme = {
  colors: colors,
  fonts: fonts,
  global: {
    a: {
      color: colors.quartenary
    },
    iframe: {
      border: `5px solid ${colors.quartenary}`
    },
    img: {
      border: `5px solid ${colors.quartenary}`,
      maxWidth: "100%"
    },
    li: {
      fontSize: "2rem",
      textAlign: "left"
    },
    p: {
      color: colors.tertiary
    }
  },
  progress: {
    bar: {
      bar: {
        background: colors.secondary
      }
    }
  },
  components: {
    blockquote: {
      color: colors.tertiary
    },
    quote: {
      color: colors.quartenary,
      borderLeft: "4px solid " + colors.quartenary
    },
    cite: {
      color: colors.secondary
    }
  }
};

export default mergeObject(defaultTheme, customTheme);
