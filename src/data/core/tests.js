import React from "react";
import PageTitle from "../../pages/PageTitle";
import PageQuote from "../../pages/PageQuote";
import PageImage from "../../pages/PageImage";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";

import douter from "./images/tests/00_douter.jpg";
import types from "./images/tests/01_types.png";
import tdd from "./images/tests/02_tdd.png";
import bdd from "./images/tests/03_bdd.png";
import PR from "./images/tests/04_PR.png";

const images = { douter, types, tdd, bdd, PR };

export default [
  {
    transition: "zoom",
    p: <PageTitle title="Frontend Testing" subtitle="initiation" />
  },
  {
    p: (
      <PageQuote
        quote="“Before software can be reusable it first has to be usable.”"
        cite="Ralph Johnson (Design Patterns)"
      />
    )
  },
  {
    p: <PageImage title="Tester c'est douter ?" image={images.douter} />
  },
  {
    p: (
      <PageQuote
        quote="“Quality is never an accident it is always the result of intelligent effort.”"
        cite="John Ruskin (écrivain, poète, peintre ...)"
      />
    )
  },
  {
    p: (
      <PageList
        title="Les types de test"
        listTextSize="1.8rem"
        marginItem={20}
        points={[
          <span>
            <b>Unitaire:</b> <br />Tester des morceaux de code isolés
            (fonctions)
          </span>,
          <span>
            <b>Fonctionnel:</b> <br />Tester automatiquement les comportements
            attendus via des scénarios.
          </span>,
          <span>
            <b>Manuel:</b> <br />Tester manuellement les comportements attendus
            dans les spécifications.
          </span>
        ]}
      />
    )
  },
  {
    p: <PageImage title="Les types de test" image={images.types} />
  },
  {
    p: (
      <PageList
        title="Méthodologie"
        listTextSize="1.8rem"
        marginItem={20}
        points={[
          <span>
            <b>TDD:</b> développement piloté par les tests
          </span>,
          <span>
            <b>BDD:</b> développement piloté par le fonctionnel
          </span>,
          <span>
            <b>Code review:</b> relecture de code manuelle
          </span>
        ]}
      />
    )
  },
  {
    p: <PageImage title="Test Driven Development" image={images.tdd} />
  },
  {
    p: <PageImage title="Behavior driven development" image={images.bdd} />
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://help.github.com/articles/about-pull-requests/">
            Code review
          </Link>
        }
        image={images.PR}
      />
    )
  },
  {
    p: (
      <PageTerminal
        title="Let's go"
        subtitle="App.test.js"
        maxHeight="200px"
        output={[
          "~ cd react-native-places",
          "~ npm test || yarn test",
          <span>
            <span style={{ color: "green" }}>PASS</span> ./App.test.js (3.843s){" "}
            <br /> <span style={{ color: "green" }}>✓</span> renders without
            crashing (95ms)
          </span>
        ]}
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://facebook.github.io/jest/">Jest</Link>}
        src="https://facebook.github.io/jest/"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Hello world"
        subtitle="unit test"
        source={`function sum(a, b) {
    return a + b;
}

it('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Matchers"
        subtitle="unit test"
        source={`it('failed ! toBe use ====', () => {
    const data = {one: 1, two: 2};
    expect(data).toBe({one: 1, two: 2});
});

it('Success ! toEqual test recursive properties', () => {
    const data = {one: 1, two: 2};
    expect(data).toEqual({one: 1, two: 2});
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Matchers"
        subtitle="unit test"
        source={`it('null', () => {
    const n = null;
    expect(n).toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).not.toBeTruthy();
    expect(n).toBeFalsy();
});

it('zero', () => {
    const z = 0;
    expect(z).not.toBeNull();
    expect(z).toBeDefined();
    expect(z).not.toBeUndefined();
    expect(z).not.toBeTruthy();
    expect(z).toBeFalsy();
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Numbers"
        source={`it('two plus two', () => {
    const value = 2 + 2;
    expect(value).toBeGreaterThan(3);
    expect(value).toBeGreaterThanOrEqual(3.5);
    expect(value).toBeLessThan(5);
    expect(value).toBeLessThanOrEqual(4.5);

    // toBe and toEqual are equivalent for numbers
    expect(value).toBe(4);
    expect(value).toEqual(4);
});

it('adding floating point numbers', () => {
    const value = 0.1 + 0.2;
    expect(value).not.toBe(0.32);    // It isn't! Because rounding error
    expect(value).toBeCloseTo(0.3); // This works.
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Strings"
        source={`it('there is no I in team', () => {
    expect('team').not.toMatch(/I/);
});

it('but there is a "stop" in Christoph', () => {
    expect('Christoph').toMatch(/stop/);
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Array"
        source={`const shoppingList = [
    'diapers',
    'kleenex',
    'trash bags',
    'paper towels',
    'beer',
];

it('the shopping list has beer on it', () => {
    expect(shoppingList).toContain('beer');
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Exception"
        source={`function compileAndroidCode() {
    throw 'you are using the wrong JDK';
}

it('compiling android goes as expected', () => {
    expect(compileAndroidCode).toThrow();

    // You can also use the exact error message or a regexp
    expect(compileAndroidCode).toThrow('you are using the wrong JDK');
    expect(compileAndroidCode).toThrow(/JDK/);
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Asynchronous"
        source={`function fetchData(callback){
    setTimeout(()=>{
        callback('peanut butter');
    },200);
}

it('the data is peanut butter', done => {
    function callback(data) {
        expect(data).toBe('peanut butter');
        done();
    }

    fetchData(callback);
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Setup"
        source={`beforeEach(() => {
    initializeCityDatabase();
});

afterEach(() => {
    clearCityDatabase();
});

describe('city database ', () => {
    it('has Vienna', () => {
        expect(isCity('Vienna')).toBeTruthy();
    });

    it('has San Juan', () => {
        expect(isCity('San Juan')).toBeTruthy();
    });
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Mock"
        source={`jest.mock('react-native-i18n', () => {
    const i18njs = require('i18n-js'); 
    const fr = require('./i18n/locales/fr'); 
    i18njs.translations = { fr };
    return {
        t: jest.fn((k, o) => i18njs.t(k, o)),
        locale: 'fr-FR',
    };
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/jest/docs/en/expect.html">
            Jest doc
          </Link>
        }
        subtitle="Etc."
        src="https://facebook.github.io/jest/docs/en/expect.html"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://facebook.github.io/jest/docs/en/snapshot-testing.html">
            Snapshot testing
          </Link>
        }
        src="https://facebook.github.io/jest/docs/en/snapshot-testing.html"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Button snapshot"
        subtitle="src/shared-ui/Button.test.js"
        source={`import React from "react";
import Button from "./Button";
import renderer from "react-test-renderer";

it("Button renders correctly", () => {
    const tree = renderer.create(<Button label="OK" />).toJSON();
    expect(tree).toMatchSnapshot();
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Button snapshot"
        subtitle="src/shared-ui/Button.test.js"
        source={`import React from "react";
import Button from "./Button";
import renderer from "react-test-renderer";

describe("Button", () => {
    it("renders correctly with label", () => {
        const tree = renderer.create(<Button label="OK" />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it("renders correctly without label", () => {
        const tree = renderer.create(<Button />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});`}
        lang="jsx"
      />
    )
  },
  {
    p: (
      <PageCodePane
        title="Places reducer snapshot"
        subtitle="src/redux/reducers/places.test.js"
        source={`import places from "./places";
import actionTypes from "../actionTypes";

describe("places reducer", () => {
    it("snapshot test must return initialState", () => {
        expect(places()).toMatchSnapshot();
    });

    it(\`snapshot test on action \${actionTypes.ADD_PLACE}\`, () => {
        expect(
            places(undefined, { type: actionTypes.ADD_PLACE, label: "Paris" })
        ).toMatchSnapshot();
    });

    it(\`snapshot test on action \${actionTypes.TOGGLE_PLACE}\`, () => {
        // TODO
    });
    
});`}
        lang="jsx"
      />
    )
  }
];
