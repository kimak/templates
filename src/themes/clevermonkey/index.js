import screen from "./screen";
import print from "./print";

require("../index.css");

const styles = {
  screen,
  print
};

export default styles;
