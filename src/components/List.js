import React from "react";
import { Appear, List, ListItem } from "spectacle";

const AppearList = ({
  values,
  textSize = "2.66rem",
  margin = 0,
  appear = true
}) => (
  <List>
    {values.map((item, index) => {
      const getListItem = () => (
        <ListItem textColor="tertiary" textSize={textSize} margin={margin}>
          {item}
        </ListItem>
      );
      return (
        <div key={index}>
          {appear && <Appear transitionDuration={300}>{getListItem()}</Appear>}
          {!appear && getListItem()}
        </div>
      );
    })}
  </List>
);

export default AppearList;
