// Import React
import React from "react";
import { Heading } from "spectacle";
export default function Title({
  value,
  textColor = "tertiary",
  caps = true,
  fit = true,
  size = 1
}) {
  return (
    <Heading size={size} textColor={textColor} fit={fit} caps={caps}>
      {value}
    </Heading>
  );
}
