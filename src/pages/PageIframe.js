import React from "react";
import TitleHead from "../components/TitleHead";

export default function PageIframe({
  title,
  subtitle,
  src,
  width = "1000px",
  height = "600px",
  style = {},
}) {
  return (
    <div>
      <TitleHead value={title} />
      <iframe
        style={{ width, height, ...style }}
        frameBorder="0"
        allowFullScreen=""
        src={src}
      />
      {!!subtitle && <TitleHead size={4} value={subtitle} />}
    </div>
  );
}
