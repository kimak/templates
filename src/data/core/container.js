import React from "react";
import { S, Layout, Fill } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageList from "../../pages/PageList";
import PageTitle from "../../pages/PageTitle";
import PageCodePane from "../../pages/PageCodePane";
import Link from "../../components/Link";

import codes from "./codes/redux.code";

import separation from "./images/redux/00_separation.png";
import container from "./images/redux/01_container.png";
import components from "./images/redux/02_components.png";
import shared from "./images/architecture/03_share.png";

const images = {
  separation,
  container,
  components,
  shared
};

export default [
  {
    p: (
      <PageImage
        title="Container & Presentational"
        subtitle="Components"
        image={images.separation}
      />
    )
  },
  {
    p: (
      <div>
        <Layout>
          <Fill>
            <img
              src={images.container}
              style={{ display: "inline-block", margin: "10px" }}
            />
            <PageList
              listTextSize="2rem"
              marginItem={10}
              points={[
                "How things work",
                "Provide data",
                "Manage State",
                "No Styles"
              ]}
            />
          </Fill>
          <Fill>
            <img
              src={images.components}
              style={{ display: "inline-block", margin: "10px" }}
            />
            <PageList
              listTextSize="2rem"
              marginItem={10}
              points={[
                "How things look",
                "No app dependencies",
                "Just props and callbacks",
                "Have only UI state"
              ]}
            />
          </Fill>
        </Layout>
      </div>
    )
  },
  {
    p: (
      <PageImage
        subtitle="Exemple d'architecture web et mobile"
        title="Modules redux 'partagés'"
        image={images.shared}
      />
    )
  }
];
