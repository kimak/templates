import React from "react";
import TitleHead from "../components/TitleHead";
import List from "../components/List";

export default function PageList({
  title,
  points,
  listTextSize,
  marginItem,
  appear = true
}) {
  return (
    <div>
      <TitleHead value={title} />
      <List
        values={points}
        textSize={listTextSize}
        margin={marginItem}
        appear={appear}
      />
    </div>
  );
}
