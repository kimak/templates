// Import React
import React from "react";
import TitleHead from "../components/TitleHead";
import { Markdown } from "spectacle";

export default function PageImage({ title, subtitle = "", subelement, image }) {
  return (
    <div>
      <TitleHead value={title} />
      <Markdown
        source={`
  ![](${image})

  ${subtitle}`}
      />
      {subelement && subelement}
    </div>
  );
}
