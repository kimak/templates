const coreConcept = `{
    places: [
      {label: "Hong Kong", visited: true, id: 0},
      {label: "Londres", visited: true, id: 1},
      {label: "Singapour", visited: true, id: 2},
      {label: "Bangkok", visited: true, id: 3},
      {label: "Paris", visited: false, id: 4},
      {label: "Macao", visited: false, id: 5},
      {label: "Dubai", visited: false, id: 6},
      {label: "Shenzhen", visited: false, id: 7},
    ],
    filter: "all",
  }`;

const dispatch = `dispatch({ type: 'ADD_PLACE', label: 'New York' });
dispatch({ type: 'TOGGLE_PLACE', index: 1 });
dispatch({ type: 'SET_FILTER', filter: 'complete' });`;

const reducer = `// the places reducer receive all actions and return a new places state
function places(state = [], action) {
  switch (action.type) {
    case 'ADD_PLACE':
        return state.concat([{ label: action.label, id: state.length, visited: false }]);
    case 'TOGGLE_PLACE':
        const newState = state.concat();
        newState[action.index].visited = !newState[action.index].visited;
        return newState;
    default:
        return state;
    }
}`;

const filter = `// the filter reducer receive all actions and return a new filter state
function placesFilter(state = 'all', action) {
  if (action.type === 'SET_FILTER') {
    return action.filter;
  } else {
    return state;
  }
}`;

const rootReducer = `function rootReducer(state = {}, action) {
    return {
        places: places(state.places, action),
        placesFilter: placesFilter(state.placesFilter, action)
    };
}`;

const createStore = `import { combineReducers, createStore } from 'redux';
import { placesFilter } from './redux/reducers/placesFilter';
import { places } from './redux/reducers/places';

const reducer = combineReducers({ placesFilter, places });
const store = createStore(reducer);

export default store`;

const storeDispatch = `store.dispatch({
    type: 'TOGGLE_PLACE',
    index: 1
  })
  
  store.dispatch({
    type: 'SET_FILTER',
    filter: 'new'
  })`;

const getState = `console.log(store.getState())
/* Prints
{
  filter: 'all',
  places: [
    {
      label: 'Paris',
      visited: true,
    },
    {
      label: 'New York',
      visited: false
    }
  ]
}*/`;

const reactRedux = `import { connect } from 'react-redux'

function mapDispatchToProps(dispatch) {
  return {
    onSelectPlace: (index) => {
      dispatch({ type: 'TOGGLE_PLACE', index});
    }
  }
}

function mapStateToProps(state) {
  return {
    places: state.places
  }
}

const Container = connect(
  mapStateToProps,
  mapDispatchToProps
)(Component)

export default Container`;

const app = `import React from "react";
import { StatusBar } from "react-native";
import { Provider } from 'react-redux';
import Navigator from "./src/navigation";
StatusBar.setHidden(true);

import store from './src/redux/store';

const App = () => 
<Provider store={store}>
    <Navigator />
</Provider>;

export default App;`;

const store = `import { combineReducers, createStore } from 'redux';
import places from './reducers/places';

let rootReducer = combineReducers({ places });
let store = createStore(rootReducer);

export default store;`;

const placesReducer = `const initialState = [
{ id: 0, label: "Hong Kong", visited: true },
{ id: 1, label: "Londres", visited: true },
{ id: 2, label: "Singapour",visited: true},
{ id: 3, label: "Bangkok", visited: true },
{ id: 4, label: "Paris"},
{ id: 5, label: "Macao"},
{ id: 6, label: "Dubai"},
{ id: 7, label: "Shenzhen"},
{ id: 8, label: "New York"},
{ id: 9, label: "Istanbul"},
{ id: 10, label: "Kuala Lumpur"},
{ id: 11, label: "Antalya"}];

function places(state = initialState, action) {
    switch (action.type) {
        case "ADD_PLACE":
        const copyState = [...state];
        copyState.unshift({
            label: action.label,
            visited: false,
            id: copyState.length
        });
        return copyState;
        case "TOGGLE_PLACE":
        const newState = state.concat(); // equal-to [...state]
        newState[action.index].visited = !newState[action.index].visited;
        return newState;
        default:
        return state;
    }
}

export default places;`;

const container = `import { connect } from "react-redux";
import Component from "../components";

function mapDispatchToProps(dispatch) {
  return {
    onToggleItem: index => {
      dispatch({ type: "TOGGLE_PLACE", index });
    },
    addItem: label => {
      dispatch({ type: "ADD_PLACE", label });
    }
  };
}

function mapStateToProps(state) {
  return {
    places: state.places
  };
}

const PlacesContainer = connect(mapStateToProps, mapDispatchToProps)(Component);
export default PlacesContainer;`;

export default {
  coreConcept,
  dispatch,
  filter,
  reducer,
  rootReducer,
  createStore,
  storeDispatch,
  reactRedux,
  app,
  store,
  container,
  placesReducer,
  getState
};
