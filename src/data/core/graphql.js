import React from "react";
import { S, Markdown, Fit, Fill, Layout } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageIframe from "../../pages/PageIframe";
import PageTitle from "../../pages/PageTitle";

import Link from "../../components/Link";

import jsFatigue from "./images/graphql/01_jsFatigue.png";
import jsFatigueFatigue from "./images/graphql/02_fatigue_fatigue.png";
import comic from "./images/graphql/03_comic.png";
import side from "./images/graphql/04_side.jpg";
import hype from "./images/graphql/05_hype.png";
const images = {
  jsFatigue,
  jsFatigueFatigue,
  comic,
  side,
  hype
};

export default [
  {
    transition: "zoom",
    p: <PageTitle title="Graphql frontend" subtitle="initiation" />
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://medium.com/@ericclemmons/javascript-fatigue-48d4011b6fc4">
            Javascript Fatigue
          </Link>
        }
        image={images.jsFatigue}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="https://medium.freecodecamp.org/javascript-fatigue-fatigue-66ffb619f6ce">
            Javascript Fatigue Fatigue
          </Link>
        }
        image={images.jsFatigueFatigue}
      />
    )
  },
  {
    p: <PageImage title="Is it bad ?" image={images.comic} />
  },
  {
    p: (
      <PageImage
        title="It depends..."
        subtitle="Hype Cycle"
        image={images.hype}
      />
    )
  },
  {
    p: <PageImage title="Keep learning !" image={images.side} />
  },
  {
    p: (
      <PageIframe
        title={
          <Link href="https://www.youtube.com/watch?v=Vfr9MyihCgU">
            How to Learn how to learn ?
          </Link>
        }
        subtitle="Apprendre à apprendre"
        src="https://www.youtube.com/embed/Vfr9MyihCgU"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="http://graphql.org">graphql.org</Link>}
        subtitle="A query language for your API"
        src="http://graphql.org"
      />
    )
  },
  {
    p: (
      <PageIframe
        sutitle={
          <Link href="https://www.howtographql.com/">howtographql.com</Link>
        }
        btitle="Learn graphql"
        src="https://www.howtographql.com/"
      />
    )
  },
  {
    p: (
      <PageIframe
        title={<Link href="https://www.graph.cool">graph.cool</Link>}
        subtitle="GraphQL backend"
        src="https://www.graph.cool"
      />
    )
  }
];
