import React from "react";
import Logo from "./logo-mobile.png";
import javascript from "../core/javascript";
import esPratique from "../core/es-pratique";
import tooling from "../core/tooling";
import reactConcept from "../core/react-concept";
import react from "../core/react";
import reactAdvanced from "../core/react-advanced";
import reactNative from "../core/react-native";
import reactNavigation from "../core/react-navigation";
import architecture from "../core/architecture";
import redux from "../core/redux";
import reduxAdvanced from "../core/redux-advanced";
import fetch from "../core/fetch";
import tests from "../core/tests";
import styles from "../core/styles";
import reactNativeTools from "../core/react-native-tools";
import graphql from "../core/graphql";

import PageTitle from "../../pages/PageTitle";
import PageImage from "../../pages/PageImage";
import container from "../core/container";

const title = {
  transition: "zoom",
  p: <PageTitle title="A road to" subtitle="React Native" children={<img src={Logo} style={{border: 'none'}} />} />
};

// Architecture orientée composants
export default [
  title,
  ...reactConcept,
  ...react,
  ...reactNative,
  //...reactNativeConcept,
  //...reactNativeTools,
  //...architecture,
  //...redux,
  //...reactAdvanced,
  //...container,
  //...styles,
  //...reduxAdvanced,
  //...reactNavigation,
  //...tests
  //...graphql
];
