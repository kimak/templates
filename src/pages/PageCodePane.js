import React from "react";
import { CodePane } from "spectacle";
import TitleHead from "../components/TitleHead";
import Title from "../components/Title";

export default function PageCodePan({ title, child, source, lang, subtitle }) {
  return (
    <div>
      {!!title && <TitleHead value={title} />}
      {!!child && child}
      <div
        style={{
          marginBottom: "20px"
        }}
      >
        <CodePane source={source} lang={lang} />
      </div>
      {!!subtitle && (
        <TitleHead value={subtitle} size={3} textColor="quartenary" />
      )}
    </div>
  );
}
