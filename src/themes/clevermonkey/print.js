const colors = {
  primary: "black",
  secondary: "black",
  tertiary: "black",
  quartenary: "black"
};

const fonts = {
  primary: "Montserrat",
  secondary: "Helvetica"
};

export default {
  colors: colors,
  fonts: fonts
};
