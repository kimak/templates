import React from "react";
import { S, Layout, Fill } from "spectacle";
import PageImage from "../../pages/PageImage";
import PageTitle from "../../pages/PageTitle";
import PageQuote from "../../pages/PageQuote";
import PageList from "../../pages/PageList";
import PageTerminal from "../../pages/PageTerminal";
import PageCodePane from "../../pages/PageCodePane";
import PageIframe from "../../pages/PageIframe";
import Link from "../../components/Link";

import codes from "./codes/react.code";

import website from "./images/react/00_website.png";
import designPattern from "./images/react/01_pattern.png";
import mvc from "./images/react/02_mvc.png";
import mvvm from "./images/react/03_mvvm.png";
import react from "./images/react/04_react.png";
import reactOneReturn from "./images/react/05_one-return.png";
import platform from "./images/reactnative/01_react-platform.png";
import sitesReact from "./images/reactnative/02_sites-react.png";
import appReactnatives from "./images/reactnative/03_app-reactnative.png";
import poupee from "./images/react/06_poupees-russes.jpg";
import diff from "./images/react/08_diff.png";
import reconciliation from "./images/react/09_reconciliation.png";
import reconciliationKey from "./images/react/10_reconciliation2.png";
import reconciliationKeyOrder from "./images/react/10_reconciliation3.png";
import universal from "./images/react/11_universal.png";
import seo from "./images/react/11_seo.png";
import realDom from "./images/react/12_real-dom.png";
import lifecycle from "./images/react/13_lifecycle.png";
import lifecycleUpdate from "./images/react/14_lifecycle-update.png";
import todo from "./images/tooling/02_react-todo.gif";
import devtools from "./images/react/15_devtools.png";
import filter from "./images/react/16_todos-filter.gif";
import npmTrends from "./images/react/07_npm_trends.png";
import gannam from "./images/react/gangnam2.gif";

const images = {
  website,
  platform,
  sitesReact,
  appReactnatives,
  designPattern,
  mvc,
  mvvm,
  react,
  reactOneReturn,
  poupee,
  diff,
  reconciliation,
  reconciliationKey,
  reconciliationKeyOrder,
  universal,
  seo,
  realDom,
  lifecycle,
  lifecycleUpdate,
  todo,
  devtools,
  filter,
  npmTrends,
  gannam,
};

export default [
  {
    p: (
      <PageImage
        title={
          <Link href="https://reactjs.org/">
            Démarrons par React{" "}
            <span style={{ textDecoration: "line-through" }}>Native</span>
          </Link>
        }
        image={images.website}
      />
    )
  },
  /*{
    p: (
      <PageImage
        title="En développement applicatif"
        subtitle="on utilise des 'patterns' d'architecture"
        image={images.designPattern}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Model View Controller"
        subtitle="Cf. framework Backbone"
        image={images.mvc}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Model View View Model"
        subtitle="Cf. framework Angular v1"
        image={images.mvvm}
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <span>
            Ok… et React, <br />encore un nouveau framework ?
          </span>
        }
        subtitle="vraiment ?"
        image={images.react}
      />
    )
  },*/
  {
    p: (
      <PageList
        title={
          <div>
            🕚 Historique de{" "}
            <S type="bold" textColor="tertiary">
              React 
            </S>
          </div>
        }
        listTextSize="2rem"
        marginItem={20}
        points={[
          <span>2011: Jordan Walke travail sur <b>FaxJs: le futur React !</b></span>,
          <span>2012: React est <b>intégré aux Fils d’actualités</b> facebook</span>,
          <span>Mi-2012: Déployé sur <b>Instagram.com</b></span>,
          <span>2013: Rendu <b>Open Source</b> lors de la conférence JSConf U.S</span>,
          <span>2014: <b>Je commence</b> à utiliser React 🤔</span>,
          <span>2015: <b>React Native</b> mis en <b>Open Source</b> pour IOS/Android</span>,
          <Link href="https://reactjs.org/blog/2016/09/28/our-first-50000-stars.html">
            Lire l'histoire complète
          </Link>
        ]}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Sites Web en React"
        image={images.sitesReact}
        subtitle="[Et bien d'autres...](https://github.com/facebook/react/wiki/sites-using-react)"
      />
    )
  },
  {
    p: (
      <PageImage
        title={
          <Link href="//www.npmtrends.com/react-vs-@angular/core-vs-vue-vs-backbone">
            Stats npm
          </Link>
        }
        image={images.npmTrends}
      />
    )
  },
  {
    p: (
      <PageImage
        title="Popularité !== qualité"
        subtitle="« Gangnam Style » en 2012 +1 milliard de vues"
        image={images.gannam}
      />
    )
  },
  {
    transition: "zoom",
    p: <PageTitle title="Pourquoi React" subtitle="est différent ?" />
  },
  {
    p: (
      <PageQuote
        title="React n’est pas un framework !"
        quote={<span>« Many people choose  <br />to think of React as <br /> the V in MVC »</span>}
        cite="React est une librairie"
      />
    )
  },
  /*{
    p: (
      <PageImage
        title="Applications React Native"
        image={images.appReactnatives}
        subtitle="[Et plus encore...](https://facebook.github.io/react-native/showcase.html)"
      />
    )
  },*/
  {
    p: (
      <PageCodePane
        title="Une idée simple."
        subtitle="« HTML » et Js dans un même fichier."
        lang="jsx"
        source={codes.helloReact}
      />
    )
  },
  /*{
    p: (
      <PageQuote
        title="React est devenu un écosystème"
        quote="« We built React to solve one problem: building large applications with data that changes over time. »"
        cite="Core team @Facebook"
      />
    )
  },*/
  /*{
    p: (
      <div>
        <PageCodePane
          title="« En React TOUT est composant »"
          child={
            <img src={images.poupee} style={{ border: "none" }} />}
          lang="jsx"
          source={codes.composants}
        />
      </div>
    )
  },*/
  /*{
    p: (
      <div>
        <Layout>
          <Fill>
            <img src={images.legoCoupling} style={{ border: "none" }} />
          </Fill>
          <Fill>
            <img src={images.legoCohesion} style={{ border: "none" }} />
          </Fill>
        </Layout>
        <PageList
          title={
            <div>
              React apporte des solutions. <br />Il favorise le faible couplage{" "}
              <br />et une forte cohésion.
            </div>
          }
          listTextSize="2rem"
          marginItem={10}
          points={[
            "Un composant React est réutilisable",
            "Un composant React est composable",
            "Un composant React est unitairement testable"
          ]}
        />
      </div>
    )
  }*/
];
