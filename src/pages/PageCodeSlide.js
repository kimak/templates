import React, { Component } from "react";
import { CodePane, Slide } from "spectacle";
import CodeSlide from "spectacle-code-slide";
import TitleHead from "../components/TitleHead";

function getPrintableCode({
  title,
  source,
  sourcePrint,
  lang,
  transition,
  key
}) {
  return (
    <Slide transition={transition} key={title}>
      {!!title && <TitleHead value={title} />}
      <CodePane source={sourcePrint || source} lang={lang} />
    </Slide>
  );
}

function PageCodeSlide({
  title,
  source,
  lang,
  transition = "zoom",
  ranges,
  key,
  sourcePrint
}) {
  const isPrint = window.location.href.indexOf("print") !== -1;
  if (isPrint) {
    return getPrintableCode({
      title,
      source,
      sourcePrint,
      lang,
      transition,
      key
    });
  } else {
    if (ranges && ranges[0]) ranges[0].title = title;
    return (
      <CodeSlide
        showLineNumbers={false}
        transition={transition}
        key={title}
        lang={lang}
        code={source}
        ranges={ranges}
      />
    );
  }
}

export default PageCodeSlide;
