import React from "react";
export default function Title({ href, target = "_blank", children }) {
  return (
    <a href={href} target={target}>
      {children}
    </a>
  );
}
